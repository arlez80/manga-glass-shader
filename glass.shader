/*
	漫画風ガラスシェーダー by あるる（きのもと 結衣）
	Cartoon-esque Glass Shader by Yui Kinomoto @arlez80

	MIT License
*/

shader_type spatial;

const float PI = 3.1415926535;

uniform vec4 light_color : hint_color = vec4( 0.3, 0.6, 1.0, 0.25 );
uniform vec4 dark_color : hint_color = vec4( 0.23, 0.0, 0.95, 0.95 );
uniform float rim_power : hint_range( 3.0, 4.0 ) = 3.5;

varying vec3 local_vertex;

void vertex( )
{
	local_vertex = VERTEX;
}

void fragment( )
{
	float soft_rim = 1.0 - clamp( dot( normalize( VIEW ), NORMAL ), 0.0, 1.0 );
	float hard_rim = round( soft_rim );
	float view_y = clamp( ( VIEW.y + 1.0 ) * 0.5, 0.0, 1.0 );

	vec4 color = mix( dark_color, light_color, view_y );
	EMISSION = ( color.rgb * mix( hard_rim, soft_rim, view_y ) * ( rim_power * view_y ) ).rgb;
	float inner_rim = 1.5 + clamp( dot( normalize( VIEW ), NORMAL ), 0.0, 1.0 );
	ALBEDO = color.rgb * pow( inner_rim, 0.7 );
	ALPHA = color.a;
}
